import React, { useState, useEffect, useCallback } from 'react';
import { FlatList } from 'react-native';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Actions from '../redux/actions.js';
import Tile from './tile';

const ViewList = styled.View`
    flex: 1;
`;

const List = (props) => {
    const { cityWeatherList, getCityWeatherList } = props;
    const [ refreshing, setRefreshing ] = useState(false);
    const [flag, setFlag] = useState(1);

    const fetchData = useCallback(() => {
        setRefreshing(true);
        return getCityWeatherList();
    }, [flag]);

    useEffect(() => {
        fetchData().then(() => {
            setRefreshing(false);
        });
    }, [fetchData]);

    return (
        <ViewList>
            <FlatList data={cityWeatherList} renderItem={(item) => <Tile data={item.item} />} onRefresh={() => setFlag(flag + 1)} refreshing={refreshing} />
        </ViewList>
    );
};

const mapStateToProps = (state) => {
    return {
        cityWeatherList: state.cityWeatherList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getCityWeatherList () {
            return dispatch(Actions.getCityWeatherList());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(List);

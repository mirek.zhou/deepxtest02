import React, { useState } from 'react';
import {Image} from 'react-native';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Actions from '../redux/actions.js';
import Utils from '../utils/useInterval';
import AwesomeButtonBlue from 'react-native-really-awesome-button/src/themes/blue';

const ViewTile = styled.View`
    height: 120px;
    line-height: 200px;
    background-color: yellow;
    text-align: center;
    position: relative;
    margin-bottom: 10px;
`;

const TextCityName = styled.Text`
    line-height: 20px;
    position: absolute;
    left: 10px;
    top: 10px;
    font-size: 20px;
    font-weight: bold;
`;

const TextTemperature = styled.Text`
    line-height: 20px;
    position: absolute;
    left: 10px;
    bottom: 10px;
    font-size: 16px;
    font-weight: bold;
`;

const TextDescription = styled.Text`
    line-height: 20px;
    position: absolute;
    right: 10px;
    top: 10px;
    font-size: 14px;
    font-weight: bold;
`;

const ViewImage = styled.View`
    width: 100px;
    height: 60px;
    line-height: 60px;
    position: absolute;
    left: 50%;
    top: 50%;
    margin-left: -10px;
    margin-top: -30px;
`;

const TextTimer = styled.Text`
    font-size: 30px;
    font-weight: bold;
    width: 100px;
    height: 60px;
    line-height: 60px;
    position: absolute;
    left: 50%;
    top: 50%;
    margin-left: -50px;
    margin-top: -30px;
`;

const TextInterval = styled.Text`
    line-height: 20px;
    position: absolute;
    left: 10px;
    top: 30px;
    font-size: 14px;
`;

const ViewButton = styled.View`
    line-height: 20px;
    position: absolute;
    right: 10px;
    bottom: 10px;
    font-size: 16px;
`;

const Tile = (props) => {
    const { removeCityWeatheritem } = props;
    const [count, setCount] = useState(0);
    const interval = props.data.interval ? props.data.interval : 2000;
    
    Utils.useInterval(() => {
        setCount(count + 1);
    }, interval);
 
    const removeItem = () => {
        removeCityWeatheritem(props.data.name);
    };

    return (
        <ViewTile>
            <TextCityName>{props.data.name}</TextCityName>
            <TextTemperature>{props.data.main.temp} ℃</TextTemperature>
            <TextDescription>{props.data.weather[0].description}</TextDescription>

            <ViewImage>
                <Image source={{uri: `http://openweathermap.org/img/wn/${props.data.weather[0].icon}@2x.png`}} style={{height:50, width:50}} />
            </ViewImage>

            <ViewButton>
                <AwesomeButtonBlue type="primary" onPress={removeItem} size="small">Remove</AwesomeButtonBlue>
            </ViewButton>
            
            <TextTimer>{count}</TextTimer>
            <TextInterval>(interval: {interval/1000}s)</TextInterval>
        </ViewTile>
    );
};

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {
        removeCityWeatheritem (name) {
            dispatch(Actions.removeCityWeatheritem(name));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Tile);
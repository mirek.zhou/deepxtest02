import styled from 'styled-components/native';

const PageHeader = styled.View`
    background-color: green;
    width: 100%;
    height: 100px;
    align-items: center;
    justify-content: center;
    padding-top: 20px;
`;

export default PageHeader;
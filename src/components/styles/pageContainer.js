import styled from 'styled-components/native';

const PageContainer = styled.View`
    background-color: papayawhip;
    height: 100%;
    width: 100%;
    padding-bottom: 20px;
    position: relative;
`;

export default PageContainer;
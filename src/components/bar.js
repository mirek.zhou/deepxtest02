import React, {useState} from 'react';
import { Picker, Button, Text } from 'react-native';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Actions from '../redux/actions.js';
import AwesomeButtonBlue from 'react-native-really-awesome-button/src/themes/blue';

const ViewBar = styled.View`
    background-color: #16ab8b;
    width: 100%;
    text-align: center;
    position: absolute;
    top: 100px;
    bottom: 0;
    padding: 20px;
`;

const ViewFormItem = styled.View`
    margin-bottom: 20px;
`;

const TextTitle = styled.Text`
    height: 20px;
`;

const TextPicker = styled.Text`
    background-color: #FFF;
    height: 35;
    line-height: 35;
    padding-left: 10;
    border-radius: 5;
`;

const InputCtrl = styled.TextInput`
    border-color: rgba(0,0,0,.1);
    background-color: #FFF;
    border-radius: 5px;
    height: 35px;
    padding-left: 10px;
`;

const ButtonAdd = styled.Button`
    background-color: #3a7de8;
    color: #FFF;
    height: 30px;
    line-height: 30px;
    font-size: 14px;
    width: 120px;
`;

const ViewPicker = styled.View`
    position: absolute;
    bottom: 0;
    left: 50;
    right: 50;
`;

const ViewButtonOk = styled.View`
    font-size: 18px;
    font-weight: bold;
    height: 40px;
`;

const Bar = (props) => {
    let [city, setCity] = useState('');
    const [interval, setInterval] = useState('1000');
    const [showPicker, setShowPicker] = useState(false);
    const { addCityWeatheritem, cityWeatherList } = props;

    const handleCityChange = (value) => {
        setCity(value);
    };

    const handleIntervalChange = (itemValue, itemIndex) => {
        setInterval(itemValue);
    };

    const addItem = () => {
        if (!city) {
            return;
        }

        if (cityWeatherList.length >= 5) {
            setCity('');
            alert('Can not add anymore!');
            return;
        }

        if (cityWeatherList.findIndex(item => item.name.toLowerCase() === city) !== -1) {
            setCity('');
            alert('City already exist!');
            return;
        }

        addCityWeatheritem(city, Number(interval));
        setCity('');
        setInterval('1000');
    };

    const MyPicker = (props) => {
        return !props.show ? 
                [] :
                <ViewPicker>
                    <ViewButtonOk style={{flexDirection: 'row-reverse'}}>
                        <Button type="secondary" onPress={() => setShowPicker(false)} title="OK" />
                    </ViewButtonOk>

                    <Picker selectedValue={interval} onValueChange={handleIntervalChange}>
                        <Picker.Item label="1 secs" value="1000" />
                        <Picker.Item label="5 secs" value="5000" />
                        <Picker.Item label="10 secs" value="10000" />
                        <Picker.Item label="15 secs" value="15000" />
                    </Picker>
                </ViewPicker>;
    };

    return (
        <ViewBar>
            <ViewFormItem>
                <TextTitle>Enter city name : {city}</TextTitle>
                <InputCtrl value={city} onChangeText={handleCityChange} />
            </ViewFormItem>

            <ViewFormItem>
                <TextTitle>Pick an interval :</TextTitle>
                <TextPicker onPress={() => setShowPicker(true)}>{interval/1000}S</TextPicker>
            </ViewFormItem>

            <AwesomeButtonBlue type="primary" onPress={addItem} stretch="true">Add</AwesomeButtonBlue>
            <MyPicker show={showPicker} />
        </ViewBar>
    );
};

const mapStateToProps = (state) => {
    return {
        cityWeatherList: state.cityWeatherList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addCityWeatheritem (name, interval) {
            dispatch(Actions.addCityWeatheritem(name, interval));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Bar);
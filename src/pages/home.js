import React from 'react';
import styled from 'styled-components/native';
import List from '../components/list.js';
import PageContainer from '../components/styles/pageContainer';
import PageHeader from '../components/styles/pageHeader';

const TextTitle= styled.Text`
    fontSize: 20; 
    fontWeight: bold; 
    color: #FFF;
`;

const Home = (props) => {
    return (
        <PageContainer>
            <PageHeader>
                <TextTitle>Weather Report</TextTitle>
            </PageHeader>
            
            <List />
        </PageContainer>
    );
};

export default Home;

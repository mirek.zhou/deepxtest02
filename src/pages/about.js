import React from 'react';
import styled from 'styled-components/native';
import Bar from '../components/bar.js';
import PageContainer from '../components/styles/pageContainer';
import PageHeader from '../components/styles/pageHeader';

const TextTitle= styled.Text`
    fontSize: 20; 
    fontWeight: bold; 
    color: #FFF;
`;

const About = () => {
    return (
        <PageContainer>
            <PageHeader>
                <TextTitle>Weather Report</TextTitle>
            </PageHeader>
            
            <Bar />
        </PageContainer>
    );
};

export default About;
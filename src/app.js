import React from 'react';
import { createAppContainer } from 'react-navigation';
import Router from './router/index.js';
import { Provider } from 'react-redux';
import store from './redux/store';

const App = () => {
    const Navigation = createAppContainer(Router);

    return (
        <Provider store={store}>
            <Navigation></Navigation>
        </Provider>
    );
};

export default App;
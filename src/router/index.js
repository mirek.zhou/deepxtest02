import React from 'react';
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import Icon from 'react-native-vector-icons/MaterialIcons';
import Home from '../pages/home';
import About from '../pages/about';

export default createMaterialBottomTabNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            title: 'Home',
            tabBarIcon: (data) => {
                return data.focused ? <Icon name="home" size={30} color="#FFF"/> : <Icon name="home" size={30} color="#3e2465"/>;
            }
        }
    },

    About: {
        screen: About,
        navigationOptions: {
            title: 'About',
            tabBarIcon: (data) => {
                return data.focused ? <Icon name="people" size={30} color="#FFF"/> : <Icon name="people" size={30} color="#3e2465"/>;
            }
        }
    }
}, {
    initialRouteName: 'Home',
    activeColor: '#f0edf6',
    inactiveColor: '#3e2465',
    barStyle: { backgroundColor: '#694fad' },
});
  
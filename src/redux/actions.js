import axios from 'axios';
import listJson from '../data/list.json';
import shanghaiJson from '../data/shanghai.json';
import { NavigationActions } from 'react-navigation';

const getCityWeatherList = () => {
    return async (dispatch, getState) => {
        let res = {};

        res.data = listJson;
        dispatch({ type: 'SET_CITY_WEATHER_LIST', data: res.data.list });
    };
};

const removeCityWeatheritem = (name) => {
    return (dispatch, getState) => {
        let arr = getState().cityWeatherList;
        
        arr.splice(arr.findIndex(item => item.name === name), 1);
        dispatch({ type: 'SET_CITY_WEATHER_LIST', data: [].concat(arr) });
    };
};

const addCityWeatheritem = (name, interval) => {
    return async (dispatch, getState) => {
        let arr = getState().cityWeatherList;
        let res = {};

        res.data = shanghaiJson;
        res.data.name = name;
        res.data.interval = interval;
        
        dispatch({ type: 'SET_CITY_WEATHER_LIST', data: arr.concat(res.data) });
        NavigationActions.navigate({ routeName: 'Home' });
    };
};

export default {
    getCityWeatherList,
    removeCityWeatheritem,
    addCityWeatheritem
};
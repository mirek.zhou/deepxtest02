## Demo: https://imgur.com/qzjWyaW

## Please be noted:
1. Only have IOS version now. (I am too lazy to make one more android version)


## About the techs

1. Hooks for components
2. Styled-components for styles
3. React-redux for state management
4. 3rd party library for the button components.
5. react-navigation-material-bottom-tabs for the Navigator.
6. react-native-vector-icons for the Icons
7. ...


## Iphone Setting
1. setting --> developer --> Allow HTTP services


## How to run the app locally

1. git clone the code
2. npm install
3. cd ios
4. pod install
5. cd ..
6. react-native run-ios


## Bug fixed records

1. react-native unlink error:
   fixed: react-native.config.js

2. http image uri not show in IOS:
   1) ios/projectname/info.plist:
        <key>NSAppTransportSecurity</key>
        <dict>
            <key>NSAllowsArbitraryLoads</key>
            <true/>
        </dict>
   2) device --> developer: Allow http services

3. Undefined symbols for architecture arm64: "_RCTSetLogFunction
   1) in XCode select the project in the left panel
   2) Click on "Build Settings"
   3) In the "Linking" section, find"Dead Code Stripping" section and change it "No"
   4) Clean and Build

4. No bundle url present when building release app
   1) Wrong: --bundle-output ./ios/bundle/index.ios.js
      Right: --bundle-output ./ios/bundle/index.ios.jsbundle

5. Failed at building: No main.jsbundle
   Just create a ios/main.jsbundle
